function [event] = convert_events(sessionpath)
% Hegedus

% Load open ephys events file
[data, timestamps, info] = load_open_ephys_data([sessionpath '\' 'all_channels.events']);

% Find bpod TTLs
inx = data==3 & info.eventId==1;
finx = find(inx);
lfinx = length(finx);
while finx(2) - finx(1) > 10   % exclude PulsePal timestamp generated by tagging session 
    finx(1) = [];              % (only 1 / tagging session in the beginning of tagging)
end
if length(finx) < lfinx - 5    % too many TTLs dropped: suspicious
    error('Error in event convertion.')
end
event = timestamps(finx);  % eventId = 1 for onsets

% Save bpod TTLs
fnm = fullfile(sessionpath,'event.mat');
save(fnm,'event');

% PulsePal TTL onset and offsets
ppinx = data==2;
pulses = timestamps(ppinx);   % PulsePal TTL's, both TTLon and TTLoff
eventIDs = info.eventId(ppinx);
pulseon = pulses(eventIDs==1);   % PlulsePal TTL onsets
pulseoff = pulses(eventIDs==0);   % PulsePal TTL offsets

% Save PulsePal TTLs
fnm = fullfile(sessionpath,'lightevent.mat');
save(fnm,'pulseon','pulseoff');